## Git Going Reference

Now that you've installed Git and have your account all set up to push and pull, you're ready to get to work! We know that remembering all these different commands is hard to do while you're actually coding,
so we created a cheat sheet you can keep close by to reference as you go. We recommend trying to memorize the Main Workflow section, and using the rest of the commands as you need them, memorizing them at your own pace.

### Main Workflow

These are the git commands you will use on a regular basis. They are written in the order in which you would use them, and as long as you're careful to stay on track, you won't need the other commands too often.

#### Check where you are in your workflow
```Shows if you have any edited files that haven't been committed or if you have any commits that haven't been pushed.```
`git status`

