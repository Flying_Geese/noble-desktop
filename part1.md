## Create a new Rails Application
We recommend that you create a folder just for your projects. This keeps our computer organized and our work separate from things like word documents and desktop files
and folders.

Open a terminal and run the following command to create directory for your Projects. If you already have a directory that you use for your projects, feel free to skip
this, but just know that whenever we mention "Projects" directory we are talking about that folder.

`mkdir Projects`

Go ahead and navigate into your projects folder.

`cd Projects`

This is the space we will use to create all of our applications. This is a Ruby on Rails course, so we will be creating Ruby on Rails applications. A lot of the work
we do with Ruby on Rails happens at the command line using a the `rails` command. As you will soon see, the `rails` command is very powerful, and comes with lots of 
options, but we will need only a few for our purposes.

Let's start by creating a new Ruby on Rails application. To do this, run the following command in your projects directory:

`rails new cookbook`

Once you hit enter, you'll notice lots of output printing to the terminal. This feedback is how you know things are running properly, or if there is an error, Rails
will do its best to tell you exactly where it occurred.


